import urllib.request
import re

def get_hands_page(url: str = 'https://hands.ru/company/about') -> str:
    """
    utf8 
    """
    opener: object = urllib.request.urlopen(url)
    data: str = opener.read().decode('utf-8')
    return data

def get_repetitors_page(url: str = 'https://repetitors.info') -> str:
    """
    windows-1251
    """
    opener: object = urllib.request.urlopen(url)
    data: str = opener.read().decode('windows-1251')
    return data

def get_numbers(html_data: str) -> list:
    long_mask = re.compile(r"(\+7|7|8)?[\s\-]?\(?[489][0-9]{2}\)?[\s\-]?[0-9]{3}[\s\-]?[0-9]{2}[\s\-]?[0-9]{2}")
    long_matches = [x.group(0) for x in long_mask.finditer(html_data)]
    short_mask = re.compile(r"[?:\s\-]?[0-9]{3}[\s\-]?[0-9]{2}[\s\-]?[0-9]{2}")
    short_matches = [x.group(0) for x in short_mask.finditer(html_data)]
    all_matches = long_matches + short_matches
    return all_matches


def get_deep_matches(matches: list) -> set:
    """
    Получить все правильные номера из исходного кода - очень погрешная задача.
    Можно удалить все короткие совпадения с одним пробелом, чтобы минимизировать погрешность.
    """
    cool_matches = set()
    for i in range(len(matches)):
        if matches[i].count(' ') != 1:
            matches[i] = re.sub('[^0-9]', '', matches[i])
            if len(matches[i]) == 7:
                cool_matches.add('8495' + matches[i])
            elif len(matches[i]) == 11:
                cool_matches.add('8' + matches[i][1:])
    return cool_matches

def main():
    hands_data = get_hands_page()
    numbers_data = get_numbers(hands_data)
    cool_hands_matches = get_deep_matches(numbers_data)
    repetitors_data = get_repetitors_page()
    numbers_data = get_numbers(repetitors_data)
    cool_repetitors_matches = get_deep_matches(numbers_data)
    cool_matches = cool_hands_matches | cool_repetitors_matches
    for number in cool_matches:
        print(number)
    
    
if __name__ == "__main__":
    main()